using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

namespace ParkingWebApi.ParkingData
{
    public class Parking
    {
        private static readonly Lazy<Parking> instanceHolder =
        new Lazy<Parking>(() => new Parking());

        private int id = 1; //парковка встановлює id для транспорту
        private List<Transport> transports = new List<Transport>(Settings.maxParkingCapacity);
        private readonly List<Transaction> register = new List<Transaction>();
        private double balance = Settings.balance;

        private Parking()
        {
            SetMakeTransactionsTimer();
            SetOneMinuteTimer();
        }

        public static Parking Instance
        {
            get { return instanceHolder.Value; }
        }

        public List<Transport> GetTransports()
        {
            return transports;
        }


        public double OneMinuteAmountOfMoney()
        {
            double sum = 0;
            foreach(var i in register)
            {
                sum += i.Money;
            }
            return sum;
        }

        void SetOneMinuteTimer()
        {
            Timer timer = new Timer(60000);
            timer.Elapsed += WriteToFile;
            timer.AutoReset = true;
            timer.Enabled = true;
        }
        
        void SetMakeTransactionsTimer()
        {
            Timer timer = new Timer(Settings.timeToPay * 1000); //секунди в мілісекунди
            timer.Elapsed += MakeTransactions;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        void MakeTransactions(Object o, ElapsedEventArgs e)
        {
            foreach (var i in transports)
            {
                register.Add(new Transaction(DateTime.Now, i.Id, i.Tarrif));

                i.ReduceMoney();

                if(i.Balance >= 0 + i.Tarrif) 
                {
                    AddMoney(i.Tarrif);
                }
            }
        }

        void AddMoney(double amount)
        {
            balance += amount;
        }

        public void AddTransport(Transport transport)
        {
            transport.Id = id;
            transports.Add(transport);
            id++;
        }

        public void RemoveTransport(int id)
        {
            foreach (var i in transports)
            {
                if (i.Id == id)
                {
                    transports.Remove(i);
                    return;
                }
            }
            Console.WriteLine("There is not such id!");
        }

        public double GetBalance()
        {
            return balance;
        }

        public string GetTransactionHistory()
        {
            string history = "";

            try
            {
                using (StreamReader sr = new StreamReader("../../Transactions.log"))
                {
                    history = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return history; 
        }

        void WriteToFile(Object o, ElapsedEventArgs e)
        {
            string text = "";

            foreach (var i in register)
            {
                text += i.ToString();
                text += "\n";
            }

            try
            {
                using (StreamWriter sw = new StreamWriter("./Transactions.log", true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            register.Clear(); // після запису в файл
        }

        public int NumberOfFreePlaces()
        {
            return Settings.maxParkingCapacity - transports.Count;
        }

        public void FindTransportToAddMoney(int id, double money)
        {
            foreach (var i in transports)
            {
                if (i.Id == id)
                {
                    i.AddMoney(money);
                    return;
                }
            }
            Console.WriteLine("There is not such id!");
        }

        public List<Transaction> GetRegister()
        {
            return register;
        }
    }
}
