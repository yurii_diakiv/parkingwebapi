using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingWebApi.ParkingData
{
    public class Transport
    {
        public double Balance { get; set; }
        public double Tarrif { get; set; }
        public int Id { get; set; }

        public Transport(double balance = 0)
        {
            if (balance < 0)
            {
                Balance = 0;
            }
            else
            {
                Balance = balance;
            }
        }

        public override string ToString()
        {
            return $"ID: {Id}; Balance: {Balance}; ";
        }

        public void AddMoney(double amountOfMoney)
        {
            Balance += amountOfMoney;
        }

        public void ReduceMoney()
        {
            if(Balance <= 0)
            {
                Balance -= Tarrif * Settings.fineCoefficient;
            }
            else
            {
                Balance -= Tarrif;
            }
        }
    }

    class Car : Transport
    {
        public Car(double balance) : base(balance)
        {
            Tarrif = Settings.tariffs["Car"];
        }

        public override string ToString()
        {
            return base.ToString() + $"Car; tarrif: {Tarrif}";
        }
    }

    class Truck : Transport
    {
        public Truck(double balance) : base(balance)
        {
            Tarrif = Settings.tariffs["Truck"];
        }

        public override string ToString()
        {
            return base.ToString() + $"Truck;  tarrif: {Tarrif}";
        }
    }

    class Bus : Transport
    {
        public Bus(double balance) : base(balance)
        {
            Tarrif = Settings.tariffs["Bus"];
        }

        public override string ToString()
        {
            return base.ToString() + $"Bus; tarrif: {Tarrif}";
        }
    }

    class Motorcycle : Transport
    {
        public Motorcycle(double balance) : base(balance)
        {
            Tarrif = Settings.tariffs["Motorcycle"];
        }

        public override string ToString()
        {
            return base.ToString() + $"Motorcycle;  tarrif: {Tarrif}";
        }
    }
}
