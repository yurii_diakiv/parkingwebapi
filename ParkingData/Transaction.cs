using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ParkingWebApi.ParkingData
{
    public class Transaction
    {
        [JsonProperty("time")]
        DateTime Time { get; set; }
        [JsonProperty("id")]
        int TransportID { get; set; }
        [JsonProperty("money")]
        public double Money { get; set; }

        public Transaction(DateTime time, int transportId, double money)
        {
            Time = time;
            TransportID = transportId;
            Money = money;
        }

        public override string ToString()
        {
            return $"Time: {Time}, Transport: {TransportID}, Money: {Money}";
        }
    }
}
