using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using ParkingWebApi.Interfaces;
using ParkingWebApi.ParkingData;

namespace ParkingWebApi.Controllers {
  [Route("api/[controller]")]
  [ApiController]
  public class TransactionController: ControllerBase {
    private ITransactionService transportService;
    
    public TransactionController(ITransactionService service) {
      transportService = service;
    }

    [HttpGet]
    public ActionResult<string> Get() {
      return Ok(transportService.GetTransactionHistory());
    }

     [HttpGet("minute")]
    public ActionResult<List<Transaction>> GetPerMinute() {
      return Ok(transportService.GetTransactionsPerMinute());
    }
  }
}