using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using ParkingWebApi.Interfaces;
using ParkingWebApi.ParkingData;

namespace ParkingWebApi.Controllers {
  [Route("api/[controller]")]
  [ApiController]
  public class TransportController: ControllerBase {
    private ITransportService transportService;
    
    public TransportController(ITransportService service) {
      transportService = service;
    }

    [HttpGet]
    public ActionResult<List<User>> Get() {
      return Ok(transportService.GetTransport());
    }

    [HttpPost]
    public IActionResult Post([FromBody] Transport newTransport) 
    {
      transportService.CreateTransport(newTransport);
      return StatusCode(201);
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        transportService.DeleteTransport(id);
        return NoContent();
    }

  }
}