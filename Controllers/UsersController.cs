using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingWebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ParkingWebApi.Controllers {
  [Route("api/[controller]")]
  [ApiController]
  public class UsersController: ControllerBase {
    private UsersService usersService;
    
    public UsersController(UsersService service) {
      usersService = service;
    }

    // GET api/values
    [HttpGet]
    public async Task<ActionResult<List<User>>> Get() {
      return Ok(await usersService.GetUsers());
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public async Task<ActionResult<User>> Get(int id) {
      return Ok(await usersService.GetUser(id));
    }
  }
}