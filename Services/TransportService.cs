using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ParkingWebApi.Interfaces;
using ParkingWebApi.ParkingData;

namespace ParkingWebApi.Services {
  public class TransportService: ITransportService {
    private ParkingWebApi.ParkingData.Parking parking;

    public TransportService() {
      parking = ParkingWebApi.ParkingData.Parking.Instance;
    }

    public List<Transport> GetTransport()
    {
        return parking.GetTransports();
    }
    public void CreateTransport(Transport newTransport)
    {
        parking.AddTransport(newTransport);
    }
    public void DeleteTransport(int id)
    {
        parking.RemoveTransport(id);
    }
  }
}