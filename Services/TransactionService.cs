using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ParkingWebApi.Interfaces;
using ParkingWebApi.ParkingData;

namespace ParkingWebApi.Services 
{
  public class TransactionService: ITransactionService 
  {
    private ParkingWebApi.ParkingData.Parking parking;

    public TransactionService()
    {
      parking = ParkingWebApi.ParkingData.Parking.Instance;
    }
    public List<Transaction> GetTransactionsPerMinute()
    {
        return parking.GetRegister();
    }
    public string GetTransactionHistory()
    {
        return parking.GetTransactionHistory();
    }
  }
}