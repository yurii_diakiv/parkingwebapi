using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using ParkingWebApi.ParkingData;

namespace ParkingWebApi.Interfaces
{
    public interface ITransportService
    {
        List<Transport> GetTransport();
        void CreateTransport(Transport newTransport);
        void DeleteTransport(int id);
    }
}