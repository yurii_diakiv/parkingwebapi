using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingWebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ParkingWebApi.Interfaces
{
    public interface IUsersService
    {
        Task<List<User>> GetUser();
        Task<User> GetUser(int id);
    }
}